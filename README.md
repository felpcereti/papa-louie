# READ FIRST
I'm not the original author of the game. \
The game was created by Flipline IDS LLC \
https://www.flipline.com/games/papalouie/info.html
# Papa Louie: When Pizzas Attack Cheat
# Cheat info
If you want to load a cheat slot you need to type
"PAPALUCCI" while holding shift in slot selection. \
It's in original game too.
## Controls:
- WASD - NoClip
- 7 - Freeze Hearts
/It doesn't work sometimes\
- 8 - Set hearts to 0
- 9 - Win Level



# Contributing and questions
Write to me on discord first
## Commit history
I included original pcode(the asm file is a pcode from JPEXS decompiler) first and then the modded code. \
But you can't view it because the changes are too big.

# Contact
Discord: felpcereti
